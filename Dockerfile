FROM manjarolinux/base

ARG AUR_PACKAGES="su-exec python3-memoizedb python3-xcgf python3-xcpf python3-aur python3-colorsysplus pm2ml pbget powerpill bauerbill lgogdownloader play.it"
ARG MANJARO_REPO_URL="http://manjaro.moson.org"

ENV PUID=1000 PGID=1000

COPY --from=jenkins/inbound-agent /usr/share/jenkins/agent.jar /usr/share/jenkins/
COPY --from=jenkins/inbound-agent /usr/local/bin/jenkins-agent /usr/local/bin/

RUN set -x \
 && cp /usr/bin/true /usr/bin/systemd-tmpfiles \
 && sed -i '/\[options\]/a NoUpgrade=usr/bin/systemd-tmpfiles' /etc/pacman.conf \
 && sed -i '/\[options\]/a HoldPkg='"$AUR_PACKAGES" /etc/pacman.conf \
 && pacman-mirrors --api --url $MANJARO_REPO_URL \
 && pacman -Syyuu --noconfirm --needed glibc \
 && pacman -S --noconfirm --needed \
      sudo \
      pigz \
      pbzip2 lbzip2 \
      git \
      mercurial \
      fakeroot \
      base-devel \
      manjaro-tools \
      wget \
      parallel \
      \
      python-gitpython \
      jq \
      bc \
      \
      icoutils \
      imagemagick \
      innoextract \
      libarchive \
      unzip \
      inetutils \
      \
      nfs-utils \
      \
      jre-openjdk-headless \
 && pacman -Scc --noconfirm \
 && rm -rf /var/cache/pacman/pkg/* \
 \
 && ( userdel -rf builder || true ) \
 && groupadd --gid "$PGID" jenkins \
 && useradd -G wheel --gid "$PGID" --uid "$PUID" -m jenkins \
 && chmod 755 ~jenkins \
 && echo "jenkins ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
 && rm -f /var/log/lastlog /var/log/faillog \
 && chmod 666 /etc/passwd

COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT [ "/entrypoint.sh" ]

USER jenkins
WORKDIR /home/jenkins

COPY *.patch ./
RUN set -x \
 && for package in $AUR_PACKAGES; do \
        git clone https://aur.archlinux.org/$package.git \
 &&     pushd $package \
 &&         [ ! -f "../${package}_pre.patch" ] || patch -p1 -i "../${package}_pre.patch" \
 &&         makepkg --skippgpcheck --noconfirm --noextract --nobuild --nodeps \
 &&         makepkg --skippgpcheck --noconfirm --nobuild --nodeps \
 &&         [ ! -f "../$package.patch" ] || patch -p1 -i "../$package.patch" \
 &&         makepkg -irs --skippgpcheck --noconfirm --noextract \
 &&      popd \
 &&     rm -rf $package \
 ;  done \
 && rm -fv *.patch \
 && which bauerbill play.it lgogdownloader \
 && rm -rf /var/cache/pacman/pkg/* \
 && rm -rf ~/.gnupg

USER root
