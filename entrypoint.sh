#!/usr/bin/bash
set -eu

JENKINS_UID=$(id -u jenkins)
JENKINS_GID=$(id -g jenkins)

if [ "$(id -u)" != '0' ]; then
    PUID=$(id -u)
    PGID=$(id -g)
fi

if [ "$PUID" != "$JENKINS_UID" ]; then
    sed "/^jenkins:/ s/$JENKINS_UID/$PUID/" /etc/passwd > /tmp/passwd
    cat /tmp/passwd > /etc/passwd
    rm /tmp/passwd
    sudo chown -R jenkins ~jenkins
fi

if [ "$PGID" != "$JENKINS_GID" ]; then
    sudo groupmod --gid $PGID --non-unique jenkins
    sudo chown -R :jenkins ~jenkins
fi

sudo chmod 644 /etc/passwd

if [ "$(id -u)" = '0' ]; then
    su-exec jenkins:jenkins /usr/local/bin/jenkins-agent "$@"
else
    exec /usr/local/bin/jenkins-agent "$@"
fi
